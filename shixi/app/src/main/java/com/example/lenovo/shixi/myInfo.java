package com.example.lenovo.shixi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class myInfo extends AppCompatActivity {
    private TextView textView1;
    private String str1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_info);
        textView1=(TextView)findViewById(R.id.t1);
        Intent intent=getIntent();
        str1=intent.getStringExtra("name");
        textView1.setText("用户名:"+str1);
    }
}
