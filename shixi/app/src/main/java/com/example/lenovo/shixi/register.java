package com.example.lenovo.shixi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONObject;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class register extends AppCompatActivity {
    private EditText name;
    private EditText pwd;
    private EditText city;
    private String scity;
    private String sName;
    private String sPwd;
    private Button button;
    private String path="http://42.159.6.14:5555/register";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Intent intent=getIntent();
        name = (EditText) findViewById(R.id.name);
        pwd  =(EditText) findViewById(R.id.pwd);
        city=(EditText)findViewById(R.id.city);
        button=(Button)findViewById(R.id.b1);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                sName = name.getText().toString();
                sPwd = pwd.getText().toString();
                scity=city.getText().toString();
                MyGetTask task = new MyGetTask();
                task.execute(path);

            }
        });
    }
    class MyGetTask extends AsyncTask<String, Void, String>{
        protected String doInBackground(String... params) {
            String url =params[0];
            try {
                JSONObject object = new JSONObject();
                object.put("name", sName);
                object.put("pwd", sPwd);
                object.put("city",scity);
                URL url1 = new URL(url);
                //得到connection对象。
                HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
                //设置请求方式
                conn.setDoOutput(true);// 允许输出
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                byte []data=(object.toString()).getBytes();
                conn.setDoOutput(true);
                conn.setUseCaches(false);
                conn.setRequestProperty("Charset", "UTF-8");
                conn.setRequestProperty("Content-Length",data.length+"");
                conn.setRequestProperty("contentType","application/json");
                conn.connect();
                OutputStream os = conn.getOutputStream();
                os.write(data);
                os.flush();
                os.close();
                String str="";
                if(conn.getResponseCode()==200){

                    InputStream is=conn.getInputStream();
                    byte [] d = new byte[conn.getContentLength()];
                    is.read(d);
                    str = new String(d);
                }
                return str;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Toast.makeText(getApplicationContext(), "result:"+result, Toast.LENGTH_SHORT).show();
            if(result.equals("注册成功")){
                Intent intent=new Intent();
                intent.setClass(register.this,login.class);
                startActivity(intent);
            }
        }
    }
}
