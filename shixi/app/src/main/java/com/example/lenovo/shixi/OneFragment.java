package com.example.lenovo.shixi;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


public class OneFragment extends Fragment {
    private ListView mListView;
    private String[] names={"达·芬奇《蒙娜丽莎》","现实主义美术典型代表《拾穗者》","印象派画家莫奈《日出·印象》","梵高心目中的《向日葵》","梵高的星空之谜"};
    private String[] content={"无数人为之倾倒的神秘微笑","米勒画中的三位农妇是法国的三位女神","对美与真实的否定，只能给人一种印象","时代变幻莫测的礼赞，充满活力而色彩孑然","躁动不安的情感和疯狂的幻觉世界"};
    private int[] icons={R.drawable.one,R.drawable.two,R.drawable.three,R.drawable.four,R.drawable.five};
    public static Context con;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_one,container, false);
        mListView=(ListView)v.findViewById(R.id.lv);
        con=v.getContext();
        MyBaseAdapter myBaseAdapter=new MyBaseAdapter();
        mListView.setAdapter(myBaseAdapter);
        return v;
    }
    class MyBaseAdapter extends BaseAdapter{
        public int getCount(){
            return names.length;
        }
        public Object getItem(int position){
            return names[position];
        }
        public long getItemId(int position){
            return position;
        }
        public View getView(int position,View convertView,ViewGroup parent){
            View view=View.inflate(con,R.layout.news_item,null);
            TextView textView1=(TextView)view.findViewById(R.id.tv_title);
            textView1.setText(names[position]);
            TextView textView2=(TextView)view.findViewById(R.id.tv_description);
            textView2.setText(content[position]);
            ImageView imageView=(ImageView)view.findViewById(R.id.siv_iocn);
            imageView.setBackgroundResource(icons[position]);
            return view;
        }
    }
}
