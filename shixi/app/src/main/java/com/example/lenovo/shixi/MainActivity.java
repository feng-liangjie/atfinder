package com.example.lenovo.shixi;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.view.View;
import android.view.MenuItem;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    private TextView textView1;
    private TextView textView2;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    textView1.setText("");
                    textView2.setText("");
                    OneFragment fragment1 = new OneFragment();
                    FragmentManager fm1 = getFragmentManager();
                    //事务
                    FragmentTransaction ft1 =  fm1.beginTransaction();
                    ft1.replace(R.id.container, fragment1);
                    ft1.commit();
                    return true;
                case R.id.navigation_dashboard:
                    textView1.setText("");
                    textView2.setText("");
                    TwoFragment fragment2 = new TwoFragment();
                    FragmentManager fm2 = getFragmentManager();
                    //事务
                    FragmentTransaction ft2 =  fm2.beginTransaction();
                    ft2.replace(R.id.container, fragment2);
                    ft2.commit();
                    return true;
                case R.id.navigation_notifications:
                    textView1.setText("");
                    textView2.setText("");
                    ThreeFragment fragment3 = new ThreeFragment();
                    FragmentManager fm3 = getFragmentManager();
                    //事务
                    FragmentTransaction ft3 =  fm3.beginTransaction();
                    ft3.replace(R.id.container, fragment3);
                    ft3.commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView1=(TextView)findViewById(R.id.t1);
        textView2=(TextView)findViewById(R.id.t2);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

}

