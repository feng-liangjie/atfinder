import  MySQLdb
class MyHelper():
    def __init__(self):
        self.host ='42.159.6.14'
        self.port = 3306
        self.db = 'app_db'
        self.user ='root'
        self.pwd ='Aa123456%'
        self.charset='utf8'
    #获取连接对象
    def connection(self):
        self.conn = MySQLdb.connect(host=self.host,
        port = self.port,db=self.db,
        user = self.user,passwd = self.pwd,charset=self.charset)
        self.csl = self.conn.cursor()
    #关闭资源
    def free(self):
        try:
             self.csl.close()
             self.conn.close()
        except Exception as e:
            print(e)
    #增加，删除，更新
    def executeUpdate(self,sql,params=[]):
        rows = 0
        try:
            self.connection()
            rows = self.csl.execute(sql,params)
            print("影响的行数：",rows)
            self.conn.commit()
            self.free()
        except Exception as e:
            print(e)
        return  rows
    #查询
    def executeQuery(self,sql,params=[]):
        result =()
        try:
            self.connection()
            self.csl.execute(sql,params)
            result = self.csl.fetchall()
            self.conn.commit()
            self.free()
        except Exception as e:
            print(e)
        return result

